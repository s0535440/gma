package de.htw_berlin.aufgabe08;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class ListDemoActivity extends ListActivity {
	private final static String TAG = ListDemoActivity.class.getSimpleName();
	private String texts[] = {"an item", "item", "anoter item", "Eintrag", "blub", "foo","bar"};

	//String contentList[] = {"1","2","3"};
	private ArrayList<String> contentList = new ArrayList<String>();
	
	private ArrayAdapter<String> adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//  Aufgabe Möglichkeit 2:
//		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//				R.array.listEntries, android.R.layout.simple_list_item_1); 
//		this.setListAdapter(adapter);

//  Aufgabe Möglichkeit 1:
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//				android.R.layout.simple_list_item_1);
//		
//		for (int i = 0; i<20; i++) {
//			adapter.add("Eintrag " +(i+1));
//		}
// ende Möglichkeit 1
		
// Möglichkeit 3 Daten aus ArrayList:	
		for (int i = 0; i<texts.length; i++) {
			contentList.add(texts[i]+" " +(i+1));
		}
		
		Collections.sort(contentList);
		//mit sort + reverse ergibt umgekehrte alphabetische Sortierung.
		//! Case sensitive! Erst Großbuchstaben dann Kleinbuchstaben
		//Collections.reverse(contentList);
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contentList);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView parent, View v, int position, long id) {
		super.onListItemClick(parent, v, position, id);
		String item = (String) parent.getItemAtPosition(position);
		Toast.makeText(this, item, Toast.LENGTH_SHORT).show();

		//z.B. item löschen:
		//contentList.remove(item);
		
		//oder item hinzufügen:
		// ein paar Einträge, damit etwas zu sortieren ist:
		contentList.add(texts[position%texts.length] +" " + position);
		//alphabetisch neu sortieren nach löschen oder hinzufügen:
		//Collections.sort(contentList);
		
		//sort reverse (one call):
//		Collections.sort(contentList, new Comparator<String>(){
//			@Override
//			public int compare(String lhs, String rhs) {
//				return rhs.compareToIgnoreCase(lhs);
//			}
//		});
		adapter.notifyDataSetChanged();
		Log.d(TAG,"clicked at " + item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_demo, menu);
		return true;
	}

}
