package de.htw_berlin.requestpermissions;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    private final String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1000;

    private boolean deniedOnePermission = true;
    private final List<String> listPermissionsNeeded = new ArrayList<>();

    private TextView helloTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helloTV = (TextView) findViewById(R.id.hello_tv);
        if (deniedOnePermission) {
            helloTV.setText("SORRY permissions not granted");
        } else {
            helloTV.setText("YEAH permissions granted");
        }

        if (deniedOnePermission) {
            checkPermissions();
        }
    }


    private void checkPermissions() {
        Log.d(TAG, "checkPermissions");
        listPermissionsNeeded.clear();
        for (String p:permissions) {
            if (ContextCompat.checkSelfPermission(this,p) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                    @NonNull int[] grantResults) {

        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
            Log.d(TAG, "onRequestPermissionResult");
            if (grantResults.length == 0) { //request was cancelled
                deniedOnePermission = true; //which should not be a change, otherwise this method will not be called
                return;
            }
            deniedOnePermission = false; //will be overridden
            List<Integer> deniedList = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                boolean currentPermissionDenied;
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "granted: " +i);
                    currentPermissionDenied = false;
                } else {
                    Log.d(TAG, "not granted: " + i);
                    currentPermissionDenied = true;
                    deniedList.add(i);
                }
                deniedOnePermission = deniedOnePermission || currentPermissionDenied;
            }
            if (!deniedOnePermission) {
                Log.d(TAG, "All permissions granted");
                helloTV.setText("YEAH permissions granted");
            } else {
                for (int i = 0; i < deniedList.size(); i++) {
                    Log.d(TAG, "permission not granted: " + permissions[deniedList.get(i)]);
                }
            }
        }
    }
}

