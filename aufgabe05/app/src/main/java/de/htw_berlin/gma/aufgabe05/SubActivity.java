package de.htw_berlin.gma.aufgabe05;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SubActivity extends AppCompatActivity implements OnClickListener{

    public static final String ANSWER_KEY = "answer";
    public static final String ORIG_QUESTION_KEY = "origquestion";

    private CharSequence origQuestion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        Button btnReply = (Button)findViewById(R.id.btn_sub_reply);
        btnReply.setOnClickListener(this);
        Bundle subActivityExtras = getIntent().getExtras();

        if ( subActivityExtras!=null && subActivityExtras.containsKey(FirstActivity.QUESTION_KEY)) {
            origQuestion = subActivityExtras.getCharSequence(FirstActivity.QUESTION_KEY);

            if ( origQuestion.toString().trim().length() > 0) {
                ((TextView)findViewById(R.id.tv_sub)).setText(origQuestion);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btn_sub_reply){
            Intent intent  = new Intent();
            intent.putExtra(ANSWER_KEY, 77);

            if (origQuestion != null && origQuestion.length()>0) {
                intent.putExtra(ORIG_QUESTION_KEY, origQuestion);
            }

            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

