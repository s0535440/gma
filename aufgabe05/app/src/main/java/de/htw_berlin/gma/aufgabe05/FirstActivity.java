package de.htw_berlin.gma.aufgabe05;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class FirstActivity extends AppCompatActivity implements OnClickListener {

    private EditText questionInputField;
    private static final int SUBACTIVITY_RESULT = 1000;

    //accessible for the SubActivity:
    public static final String QUESTION_KEY = "question";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Button btn = (Button)findViewById(R.id.btn_ask_me);
        btn.setOnClickListener(this);
        questionInputField = (EditText) findViewById(R.id.editText_question_input);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_ask_me) {
            CharSequence inputValue = questionInputField.getText();
            Toast.makeText(this,inputValue, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, SubActivity.class);
            intent.putExtra(QUESTION_KEY, inputValue);
            startActivityForResult(intent, SUBACTIVITY_RESULT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == SUBACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
            //CharSequence answer = "Die Antwort ist: " + data.getExtras().getInt("answer");
            Resources res = getResources();
            Bundle bundle = data.getExtras();
            int answerValue = 0;
            if (bundle.containsKey(SubActivity.ANSWER_KEY)) {
                answerValue = bundle.getInt(SubActivity.ANSWER_KEY);
            }
            CharSequence origQuestion = "--";
            if (bundle.containsKey(SubActivity.ORIG_QUESTION_KEY)) {
                origQuestion = bundle.getCharSequence(SubActivity.ORIG_QUESTION_KEY);
            }

            //Example how to format Strings. Also look at strings.xml.
            String answer = String.format(res.getString(R.string.answer),origQuestion, answerValue);

            Toast.makeText(this,answer, Toast.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

