package de.htw.berlin.viewext;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

public class RunnableView extends View implements Runnable {

	private static final String TAG = RunnableView.class.getSimpleName();
	private Paint brushBlue;
	private int radius ;
	boolean increment;
	boolean running;
	
	public void init () {
		brushBlue = new Paint();
		brushBlue.setColor(Color.BLUE);
		radius = 20;
		increment = true;
		
	}
	
	
	public RunnableView(Context context) {
		super(context);
		init();
	}

	@Override
	public void run() {
		Log.d(TAG, "run");
		running = true;
		while ( running) {
			if (increment) {
				++radius;
			} 
			else if ( radius>0 )  {
				--radius;
			} else {
				increment = true;
			}
			this.postInvalidate();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void terminate() {
		running = false;
		Log.d(TAG, "terminate");
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int width = getWidth();
		int height = getHeight();
		if (radius > width/2 || radius > height/2) {
			increment = false;
		}
		canvas.drawCircle(width/2, height/2, radius, brushBlue);
	}
	
	
}
