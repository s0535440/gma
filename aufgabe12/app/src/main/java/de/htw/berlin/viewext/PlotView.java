package de.htw.berlin.viewext;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class PlotView extends View {
	private static final String TAG = PlotView.class.getSimpleName();
	
	private Paint brushRed;
	private int width;
	private int height;
	
	public void init () {
		brushRed = new Paint();
		brushRed.setColor(Color.RED);
	}
	
	public PlotView(Context context) {
		super(context);
		init();
		Log.d(TAG, "ctor1");
	}
	
	public PlotView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		Log.d(TAG, "ctor2");
	}

	public PlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		Log.d(TAG, "ctor3");
	}

	@Override
	protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
		super.onSizeChanged(xNew, yNew, xOld, yOld);

		width = xNew;
		height = yNew;
	}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawCircle(width/2, height/2, 50, brushRed);
	}
	
	
}
