package de.htw.berlin.viewext;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends Activity implements OnClickListener {
	private static final String TAG = MainActivity.class.getSimpleName();
	
	private PlotView plotView;
	private RunnableView runView;
	private Thread thread;
	private Button startStopButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		LinearLayout layout = (LinearLayout) findViewById(R.id.LinearLayout1);
		plotView = new PlotView(this);
		plotView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 200));
		layout.addView(plotView);
		runView = new RunnableView(this);
		runView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 200));
		layout.addView(runView);
		startStopButton = (Button) findViewById(R.id.button1);
		startStopButton.setOnClickListener(this);
		//thread = new Thread(runView);
		//thread.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG,"onClick");
		if (v.getId() == R.id.button1) {
			Log.d(TAG,"onClick2");
			if (thread != null && thread.isAlive() ) {
				runView.terminate();
				try {
					thread.join();
					startStopButton.setText(R.string.btn_start);
				} catch( InterruptedException e) {
					e.printStackTrace();
				}
			}
			else {
				thread = new Thread(runView);
				thread.start();
				startStopButton.setText(R.string.btn_stop);
			}
		}
	}

}
