package de.htw_berlin.gma.aufgabe02;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class RandomActivity extends AppCompatActivity {

    private static final String TAG = RandomActivity.class.getSimpleName();
    private TextView tv_output;
    private Random rand;
    private EditText etMaxValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rand = new Random();
        tv_output = (TextView)findViewById(R.id.tv_output);
        etMaxValue = (EditText)findViewById(R.id.et_input);
    }

    public void onButtonClick(View v) {
        Log.d(TAG, "onButtonClick");
        if (v.getId() == R.id.main_btn_generaterandom) {
            int maxValue = 6;
            try {
                maxValue = Integer.parseInt(etMaxValue.getText().toString());
            } catch(NumberFormatException e) {
                Log.d(TAG,"caught exeption");
               // e.printStackTrace();
            }
            tv_output.setText("" + (rand.nextInt(maxValue) + 1));
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
