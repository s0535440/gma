package de.htw_berlin.gma.aufgabe02;

import android.os.Build;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import static junit.framework.Assert.assertTrue;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class RandomActivityTest {

    @Test
    public void clickingLogin_shouldStartLoginActivity() {
        RandomActivity activity = Robolectric.setupActivity(RandomActivity.class);
        TextView tv_output = (TextView) activity.findViewById(R.id.tv_output);
        for (int i  = 0; i<10; i++) {
            activity.findViewById(R.id.main_btn_generaterandom).performClick();
            int randomValue = Integer.parseInt(tv_output.getText().toString());
            assertTrue(0 < randomValue && 7 > randomValue);
        }
    }
}
