package de.htw_berlin.gma.aufgabe06;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class ImplicitIntentsActivity extends AppCompatActivity implements
        OnClickListener {

    private Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implicit_intents);

        findViewById(R.id.btn_intents_dial).setOnClickListener(this);
        findViewById(R.id.btn_intents_sms).setOnClickListener(this);
        findViewById(R.id.btn_intents_webview).setOnClickListener(this);
        findViewById(R.id.btn_intents_email).setOnClickListener(this);
        findViewById(R.id.btn_intents_card).setOnClickListener(this);
        findViewById(R.id.btn_intents_navigate).setOnClickListener(this);
        findViewById(R.id.btn_intents_streetview).setOnClickListener(this);
        res = getResources();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_implicit_intents, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_intents_dial:
                intent = new Intent(Intent.ACTION_DIAL,
                        Uri.parse("tel:+49 (221) 12345678"));
                startActivity(intent);
                break;
            case R.id.btn_intents_sms:
                intent = new Intent(Intent.ACTION_SENDTO,
                        Uri.parse("sms:+43 (699) 12345678"));
                startActivity(intent);
                break;
            case R.id.btn_intents_webview:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://developer.android.com/design/index.html"));
                startActivity(intent);
                break;
            case R.id.btn_intents_email:
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("mailto:hans@wurst.com"));
                startActivity(intent);
                break;
            case R.id.btn_intents_card:
                try {
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("geo:52.45752,13.526434?z=16"));
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(
                            this,
                            res.getString(R.string.geo_notsupported_msg),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_intents_navigate:
                try {
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=52.45752,13.526434"));
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(
                            this,
                            res.getString(R.string.navi_notsupported_msg),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_intents_streetview:
                try {
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("google.streetview:cbll=52.457503,13.527882"));
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(this,
                            res.getString(R.string.streetview_notsupported_msg),
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}