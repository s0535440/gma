package de.htw.berlin.testfragments;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class MainActivity extends Activity implements OnItemClickListener {

	private DetailFragment detailFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.listEntries, android.R.layout.simple_list_item_1);
		FragmentManager fm = getFragmentManager();
		ListFragment listFrag = (ListFragment) fm.findFragmentById(R.id.fragmentList);
		listFrag.setListAdapter(adapter);
		listFrag.getListView().setOnItemClickListener(this);
		
		FragmentTransaction ft =  fm.beginTransaction();
		detailFragment =  new DetailFragment();
		ft.add(R.id.frameLayoutFragmentContainer,detailFragment);
		ft.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int itemPosition, long itemId) {
		String text = this.getResources().getString(R.string.detailText, itemPosition+1);
		detailFragment.setDetailText( text);
	}

}
