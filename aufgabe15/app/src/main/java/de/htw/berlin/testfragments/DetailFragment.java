package de.htw.berlin.testfragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.detailfragmentlayout, container,false);
		
	}

	public void setDetailText(String text) {
		View v = getView();
		if (v != null) {
			TextView textView = (TextView)v.findViewById(R.id.textViewDetailText);
			textView.setText(text);
		}
	}
	
}
