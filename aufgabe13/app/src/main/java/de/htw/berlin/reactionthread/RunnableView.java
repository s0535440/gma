package de.htw.berlin.reactionthread;

import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class RunnableView extends View implements Runnable {

	private static final String TAG = MainActivity.class.getSimpleName();
	
	private Paint textPaint;
	private static final int[]  COLORS  = {Color.RED, Color.GREEN, Color.BLUE,  Color.BLACK};
    private Random rand;
    
    private int posX;
    private int posY;
    private boolean running;
    
	public RunnableView(Context context) {
		super(context);
	       textPaint = new Paint();
	       textPaint.setTextSize(50);
	       textPaint.setStrokeWidth(10);
	       textPaint.setColor(Color.BLACK);
	       rand = new Random();
	   	   running = true;
	}

	public void terminate(){
		running = false;
	}

	@Override
	public void run() {
		running = true;
		Log.d(TAG,"run() " + running);
		while(running) {
			textPaint.setColor(COLORS[rand.nextInt(COLORS.length)]);
			this.postInvalidate();
			//Log.d(TAG,"run()");
			try {
				Thread.sleep(2*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	    posX = getWidth() / 2 ;
	    posY = getHeight() / 2; 
		//Log.d(TAG,"onDraw() "+ posX+" " + posY);
		canvas.drawText("Text!", posX / 2, (int) (1.2 * posY), textPaint);
	}
	
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
		 int action = event.getAction();
		 if(action == MotionEvent.ACTION_DOWN) {
			 Log.d(TAG, "touched");
			 return true;
		 }
		 return super.onTouchEvent(event);    	
	 }
}
