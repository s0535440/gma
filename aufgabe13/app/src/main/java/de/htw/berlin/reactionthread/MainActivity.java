package de.htw.berlin.reactionthread;


import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends Activity {
	
	private static final String TAG = MainActivity.class.getSimpleName();
	private RunnableView rView;
	private Thread thread;
	private Button startButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		rView = new RunnableView(this); 
		LinearLayout verticalLayout = (LinearLayout) findViewById(R.id.gamepad);
        verticalLayout.addView(rView);
        startButton = (Button)findViewById(R.id.btn_start);
	}

	public void onButtonClick(View view) {
		Log.d(TAG, ".onButtonClick");
    	switch (view.getId() ) {
    	case R.id.btn_start: 
    		if (thread != null && thread.isAlive()) {
    			rView.terminate();
    			try {
					thread.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			startButton.setText(R.string.start);
    		} else {
    			thread = new Thread(rView);
    			thread.start();
    			//startButton.setClickable(false);
    			startButton.setText(R.string.stop);
    		}
    		break;
    	}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
