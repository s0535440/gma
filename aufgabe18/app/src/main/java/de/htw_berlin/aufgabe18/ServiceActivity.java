package de.htw_berlin.aufgabe18;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import de.htw_berlin.aufgabe18.MyBoundService.MyLocalBinder;


public class ServiceActivity extends AppCompatActivity {

    private MyBoundService myService;
    private boolean isBound = false;
    private TextView timeprint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        timeprint = (TextView) findViewById(R.id.timeprint);

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, MyBoundService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (isBound) {
            unbindService(mConnection);
            isBound = false;
        }
    }

    public void onClick(View view) {
        if (view.getId() == R.id.button) {
            if (isBound) {
                String currentTime = myService.getCurrentTime();
                timeprint.setText(currentTime);
            }
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MyLocalBinder binder = (MyLocalBinder) service;
            if (binder!=null) {
                myService = binder.getService();
                isBound = true;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
                isBound = false;
            }
    };
}
