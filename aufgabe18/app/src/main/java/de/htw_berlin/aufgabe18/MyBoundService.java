package de.htw_berlin.aufgabe18;

/**
 * Created by obermann on 10.10.15.
 *
 */
import android.app.Service;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import java.util.Date;
import java.util.Locale;

import java.text.SimpleDateFormat;

public class MyBoundService extends Service {

    private final IBinder myBinder = new MyLocalBinder();

    public class MyLocalBinder extends Binder {
        MyBoundService getService() {
            return MyBoundService.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    //i.e.
    public String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss MM/dd/yyyy", Locale.GERMANY);
        return dateFormat.format(new Date());
    }

}

