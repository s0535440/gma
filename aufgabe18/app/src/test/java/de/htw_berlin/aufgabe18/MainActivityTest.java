package de.htw_berlin.aufgabe18;

/**
 * Created by david.obermann on 17.04.16.
 */
// MainActivityTest.java

// Static imports for assertion methods

import android.os.Build;
import android.widget.Button;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class MainActivityTest {
    private ServiceActivity activity;

    // @Before => JUnit 4 annotation that specifies this method should run before each test is run
    // Useful to do setup for objects that are needed in the test
    @Before
    public void setup() {
        // Convenience method to run MainActivity through the Activity Lifecycle methods:
        // onCreate(...) => onStart() => onPostCreate(...) => onResume()
        activity = Robolectric.setupActivity(ServiceActivity.class);
    }

    // @Test => JUnit 4 annotation specifying this is a test to be run
    // The test simply checks that our TextView exists and has the text "Hello world!"
    @Test
    public void validateButtonText() {
        TextView timeprint = (TextView) activity.findViewById(R.id.timeprint);
        assertNotNull("Textview could not be found", timeprint);
        assertTrue("Textview contains incorrect text",
                "Hello World!".equals(timeprint.getText().toString()));

        Button button = (Button) activity.findViewById(R.id.button);
        assertNotNull("Button could not be found", button);
        assertTrue("Button contains incorrect text",
                "New Button".equals(button.getText().toString()));
    }

    @Test
    public void validateButtonClick() {
        Button button = (Button) activity.findViewById(R.id.button);
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        String firstName = shadowActivity.peekNextStartedService().getComponent().getClassName();
        //assertTrue(null == shadowActivity.getNextStartedService().getComponent().getClassName());
        System.out.println(firstName);

        button.performClick();
        String name = shadowActivity.peekNextStartedService().getComponent().getClassName();
        System.out.print(name);
        //assertEquals("de.htw_berlin.aufgabe18.MyBoundService", name);
        TextView timeprint = (TextView) activity.findViewById(R.id.timeprint);
        assertNotNull("Textview could not be found", timeprint);
        assertTrue("Textview contains incorrect text",
                "Hello World!".equals(timeprint.getText().toString()));
    }
}