package de.htw.berlin.sqlitedemo;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SQLiteDemoActivity extends Activity implements OnClickListener {

	private static String TAG = SQLiteDemoActivity.class.getSimpleName();
	private EditText etAge;
	private EditText etName;
	
	private TextView tv_selection;
	
	private DemoSQLiteOpenHelper demoSqlHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sqlite_demo);
		etAge = (EditText) findViewById(R.id.et_age);
		etName = (EditText) findViewById(R.id.et_name);
		
		Button btnInsert = (Button) findViewById(R.id.btn_insert);
		btnInsert.setOnClickListener(this);
		
		tv_selection = (TextView) findViewById(R.id.tv_selection);
		
		demoSqlHelper = new DemoSQLiteOpenHelper(this);
		String selection = demoSqlHelper.query();
		tv_selection.setText(selection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sqlite_demo, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_insert) {
			String ageStr = etAge.getText().toString();
			String nameStr = etName.getText().toString();
			
			int age = -1;
			if (ageStr.length() != 0) {
				try {
					age = Integer.parseInt(ageStr);
				} catch (Exception e) {
					Log.e(TAG, "onClick parseInt ", e);
				}
			}
			if (age >= 0 && nameStr.length() != 0 ) {
				demoSqlHelper.insert(age, nameStr);
				etAge.setText("");
				etName.setText("");
			}
			//just show content if input is empty
			String selection = demoSqlHelper.query();
			tv_selection.setText(selection);
			
		}
		
	}

}
