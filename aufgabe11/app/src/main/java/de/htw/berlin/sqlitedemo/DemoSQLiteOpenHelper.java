package de.htw.berlin.sqlitedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DemoSQLiteOpenHelper extends SQLiteOpenHelper {

	private static final String TAG = DemoSQLiteOpenHelper.class.getSimpleName();
	
	private static final String DATABASE_NAME = "demo.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String FIRSTTABLE_NAME = "firsttable";
	private static final String AGE_COLUMN = "age";
	private static final String NAME_COLUMN = "name";
	
	//SQL
	private static final String CREATE_FIRSTTABLE = 
			"CREATE TABLE " + FIRSTTABLE_NAME + 
			" (" + AGE_COLUMN + " INTEGER," + NAME_COLUMN + " TEXT)";
	
	private static final String DROP_FIRSTTABLE = 
			"DROP TABLE IF EXISTS " + FIRSTTABLE_NAME; 
	
	public DemoSQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_FIRSTTABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(TAG, "Upgrade from version: " + oldVersion + " to " + newVersion);
		clearTable(db);
	}

	public void insert( int age, String name) {
		long row = -1;
		
		try {
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(AGE_COLUMN, age);
			values.put(NAME_COLUMN, name);
			row = db.insert(FIRSTTABLE_NAME, null, values);
		} catch ( SQLiteException e) {
			Log.e(TAG, "insert error 1",e);
		} catch (Exception e) {
			Log.e(TAG, "insert error 2",e);
		} finally {
			Log.d(TAG, "inserted: " + row);
		}
	}
	
	public String query() {
		SQLiteDatabase db = getWritableDatabase();
		String q = "SELECT * FROM " + FIRSTTABLE_NAME;
		Cursor mCursor = db.rawQuery(q, null);
		StringBuilder sb = new StringBuilder();
		String name;
		String age;
		while (mCursor.moveToNext()) {
			name = mCursor.getString(mCursor.getColumnIndex(NAME_COLUMN));
			age  = mCursor.getString(mCursor.getColumnIndex(AGE_COLUMN));
            //("\"" + age + "\"  \"" + name +"\"\n" );
            sb.append("\"");
            sb.append(age);
            sb.append("\"  \"");
            sb.append(name);
            sb.append("\"\n");
		}

		mCursor.close();
		return sb.toString();
	}

	public void clearTable(SQLiteDatabase db) {
		db.execSQL(DROP_FIRSTTABLE);
		db.execSQL(CREATE_FIRSTTABLE);
	}
}
