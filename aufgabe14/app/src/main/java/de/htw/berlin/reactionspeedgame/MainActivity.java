package de.htw.berlin.reactionspeedgame;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;


/*
Die Idee zu dieser App stammt aus dem Buch von Dirk Louis/Peter Müller "Jetzt lerne ich Android".
Habe versucht das Programm aus der Erinnerung nachzuprogrammieren.
 */

public class MainActivity extends Activity {

    public static final String WAIT_TIME = "waitTime";
	public static final String RED_GREEN_IGNORE = "redGreenIgnore";
	public static final int PREFS_REQUEST_CODE = 1;
	
	private ReactionView gameview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
	    gameview = new ReactionView(this); 
	    LinearLayout verticalLayout = (LinearLayout) findViewById(R.id.linearLayoutMain0);
	    verticalLayout.addView(gameview);
	}

	public void onButtonClick(View view) {
		switch (view.getId() ) {
		case R.id.btn_start: 
			new Thread(gameview).start();
			break;
		case R.id.btn_end:
			finish();
			break;
		case R.id.btn_prefs:
			Intent intent = new Intent(this, Prefs.class);
			startActivityForResult(intent, PREFS_REQUEST_CODE);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		//super.onActivityResult(requestCode, resultCode, data);
		if (PREFS_REQUEST_CODE == requestCode &&  resultCode == Activity.RESULT_OK) 
		{
			Bundle bundle = data.getExtras();
			boolean ignoreRedGreen = bundle.getBoolean(RED_GREEN_IGNORE);
			float waitTime = bundle.getFloat(WAIT_TIME);
			saveValues(ignoreRedGreen, waitTime);
		}
	}

	private void saveValues(boolean ignoreRedGreen, float waitTime) {
		SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putBoolean(RED_GREEN_IGNORE, ignoreRedGreen);
		editor.putFloat(WAIT_TIME, waitTime);
		editor.commit();
		//editor.apply();
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
