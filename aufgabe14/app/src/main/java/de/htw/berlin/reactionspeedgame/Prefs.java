package de.htw.berlin.reactionspeedgame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

public class Prefs extends Activity {
	   public static final String  TAG= MainActivity.class.getName();
	   private Spinner spinner;
	   private CheckBox redGreenChecker;
	   
	   @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.layout_prefs);
	        spinner= (Spinner) findViewById(R.id.spinner1);
	        ArrayAdapter<CharSequence> adapter =
	        		ArrayAdapter.createFromResource(this, R.array.spinner_data,android.R.layout.simple_spinner_dropdown_item);
	        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        spinner.setAdapter(adapter);
	        spinner.setSelection(0);
	        redGreenChecker = (CheckBox) findViewById(R.id.cb_noredgreen);
	    }

	   	public void onButtonClick(View view) {
	   		if (view.getId() == R.id.btn_prefs_done) {
	   			Intent intent = new Intent();
	   			//spinner.getSelectedItemPosition()
	   			String str = (String) spinner.getSelectedItem();
	   			int pos = str.indexOf('s');
	   			String strWert = str.substring(0, pos).trim();
	   			float time = Float.valueOf(strWert);
	   			intent.putExtra( MainActivity.WAIT_TIME, time);
	   			System.out.println("Time: " + time);
	   			System.out.println("ignore RedGreen " + redGreenChecker.isChecked());
	   			intent.putExtra(MainActivity.RED_GREEN_IGNORE, redGreenChecker.isChecked() );
	   			this.setResult(Activity.RESULT_OK, intent);
	   			finish();
	   		} 
//	   		else {
//	   			Log.d(TAG, "Spinner clicked");
//	   		}
	   		
	   	}

}
