package de.htw.berlin.reactionspeedgame;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.Random;

public class ReactionView extends View implements Runnable {

	//wie ist cyan und magenta fuer leute mit rot-gruen -Schwäche?
	private final String[] colorNames 
	 		= {"rot", "grün", "cyan", "magenta", "blau", "gelb", "schwarz", "grau"};
	private final int[]    graphicsColorValues      
	  		= {Color.RED, Color.GREEN,  Color.CYAN, Color.MAGENTA, Color.BLUE, Color.YELLOW, Color.BLACK, Color.GRAY};
 
	
	private int colorIndex;
    private int nameIndex;
    private Paint tokenPaint;
    private Paint textPaint; 
	private Random randomGenerator; 
    private long lastDrawingTime;
    private SharedPreferences prefs;	
    private ToneGenerator tg;
	
    //constructor
	public ReactionView(Context context) {
		super(context);

	   	tokenPaint = new Paint();
	   	tokenPaint.setTextSize(50);
	   	tokenPaint.setStrokeWidth(10);
	   	textPaint = new Paint();
	   	textPaint.setTextSize(20);
	   	textPaint.setStrokeWidth(5);
	   	textPaint.setColor(Color.BLACK);

	   	nameIndex   = -1;
	   	colorIndex  = -1;

	  	randomGenerator    = new Random(System.currentTimeMillis());

	   	prefs = ((MainActivity) context).getPreferences(Context.MODE_PRIVATE);
	   	tg = new ToneGenerator(AudioManager.STREAM_SYSTEM, 100);
	}

	@Override  //Runnable
	public void run() {
		while(true) {
			
			boolean excludeRedGreen = prefs.getBoolean(MainActivity.RED_GREEN_IGNORE, false);
			
			if(excludeRedGreen) { //ommit red,green, cyan, magenta
				nameIndex  = 	randomGenerator.nextInt(colorNames.length-4) + 4;			
				colorIndex = 	randomGenerator.nextInt(graphicsColorValues.length-4) + 4;
			} else {
				nameIndex  = 	randomGenerator.nextInt(colorNames.length);			
				colorIndex = 	randomGenerator.nextInt(graphicsColorValues.length);
			}
				
			this.postInvalidate();
			
			try {
			    float time = prefs.getFloat(MainActivity.WAIT_TIME, 2f);
                long delay = (long)  time * 1000;
                Thread.sleep(delay);
			}
			catch(Exception ex) {
				CharSequence cs = ex.getMessage();
				Toast.makeText(this.getContext(), cs, Toast.LENGTH_LONG).show();
				
			}
		}

	}
	

    @Override //View
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction();
    	if(action == MotionEvent.ACTION_DOWN) {
    		if(nameIndex == colorIndex && nameIndex >= 0 ) {
    			alertYourTime();
    		}
    		else {
    			tg.startTone(ToneGenerator.TONE_CDMA_ALERT_AUTOREDIAL_LITE, 100);
    		}
    		return true;
    	}
  		return super.onTouchEvent(event);    	
    }

    
    private void alertYourTime() {
    	
    	long timeOfYourReaction = System.currentTimeMillis() - this.lastDrawingTime;
    	

		
    	Resources resources = getResources();
     	String message    = resources.getText(R.string.reactiontime) + ": " + timeOfYourReaction + " ms";
     	Toast.makeText(this.getContext(), message, Toast.LENGTH_SHORT).show();
//    	AlertDialog alertDialog = new AlertDialog.Builder(this.getContext()).create();
//		alertDialog.setTitle(resources.getText(R.string.result));
//		alertDialog.setMessage(message);
//     	CharSequence okMsg         = resources.getText(android.R.string.ok);
//		Handler handler =  new Handler(new MyHandler());
//		Message dummy              = Message.obtain(handler, 0);
//		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, okMsg, dummy); 
//		alertDialog.show();
    }


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		int screenWidth  = getWidth();
		int screenHeight = getHeight();

	    int posX = screenWidth / 3 ;
	    int posY = screenHeight / 2; 

	    
		
		if(nameIndex == -1) {
			canvas.drawText("Touch, wenn Farbe = Text!", posX / 2, (int) (1.2 * posY), textPaint);
		}
		else {
	       String colourName = colorNames[nameIndex];
	       int colour        = graphicsColorValues[colorIndex];
	       tokenPaint.setColor(colour);	    
	       canvas.drawText(colourName, posX, posY, tokenPaint);
	       lastDrawingTime = System.currentTimeMillis();
		}
	}
    
    //just a dummy Handler
	private class MyHandler implements Callback {

		@Override
		public boolean handleMessage(Message arg0) {
			return true;
		}
	}  

}
