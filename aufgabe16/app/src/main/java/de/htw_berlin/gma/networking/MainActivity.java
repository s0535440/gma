package de.htw_berlin.gma.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/*
 * (C) Created by David Obermann 29.5.2015, Berlin
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView textView;
    private static final String URL_GOOGLE_DE = "http://www.google.de";
    private ProgressBar progressBar;
    private static final int PROGRESSBAR_MAX = 100;
    private static final int PROGRESSBAR_MIN = 0;
    private static final int READ_TIMEOUT = 10000;
    private static final int CONNECT_TIMEOUT = 15000; /* 15s */
    private static final int RESULT_LEN_APPROXIMATE = 100000; /* 10s*/
    private static final int HUNDRED_PERCENT = 100;
    private static final int NINETY_PERCENT = 90;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        findViewById(R.id.button).setOnClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressBar.setMax(PROGRESSBAR_MAX);
        progressBar.setProgress(PROGRESSBAR_MIN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button) {

            URL url;
            try {
                url = new URL(URL_GOOGLE_DE);
            } catch (MalformedURLException e) {
                Log.d(TAG, e.getMessage());
                return;
            }
            URL[] urlsArray = {url};

            //Check connectivity to the internet:
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new DownloadTask().execute(urlsArray);
            } else {
                textView.setText("No network connection available.");
            }
        }
    }

    private String downloadUrlStream(URL url, ResultPublisher pub) throws IOException {
        Log.d(TAG, "downloadUrlStream");
        InputStream is = null;
        int resultLen;
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECT_TIMEOUT);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            resultLen = conn.getContentLength();
            Log.d(TAG, "resultLen: " + resultLen);
            //if we do not get a reliable length from the connection we use a
            // fictive number to show a progress.
            if (resultLen == -1) {
                resultLen = RESULT_LEN_APPROXIMATE;
            }

            is = conn.getInputStream();

            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            StringBuilder sb = new StringBuilder();
            String str;
            while ((str = bufferReader.readLine()) != null) {
                sb.append(str);
                //Log.d(TAG, "len: " + sb.length());
                int currentLen = sb.length() * HUNDRED_PERCENT / resultLen;

                //the percentage can only be wrong if we did not get a correct resultLen:
                pub.publishResultProgress(currentLen > HUNDRED_PERCENT ? NINETY_PERCENT : currentLen);
            }
            pub.publishResultProgress(HUNDRED_PERCENT);
            return sb.toString();
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private class DownloadTask extends AsyncTask<URL, Integer, String[]> implements ResultPublisher {

        protected String[] doInBackground(URL... urls) {
            String[] result = new String[urls.length];
            for (int i = 0; i < urls.length; i++) {
                try {
                    result[i] = downloadUrlStream(urls[i], this);

                } catch (IOException e) {
                    result[i] = "IOException caught with url: " + urls[i].toString();
                }
            }
            return result;
        }

        protected void onProgressUpdate(Integer... progress) {
            //implement your progress here
            Log.d(TAG, "onProgressUpdate: " + progress[0]);
            progressBar.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String... result) {
            textView.setText(result[0]); // show the first entry only...
            progressBar.setProgress(0); //reset progress again.
        }

        @Override
        public void publishResultProgress(int value) {
            publishProgress(value);
        }
    }

    interface ResultPublisher {
        void publishResultProgress(int value);
    }
}
