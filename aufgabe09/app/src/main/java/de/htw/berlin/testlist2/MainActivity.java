package de.htw.berlin.testlist2;

import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener, OnClickListener {
	private ListView listView1;
	private ListView listView2;
	//private String contentList1[] = { "a", "b", "c", "d", "e" };
	private ArrayList<String> contentList1 = new ArrayList<String>();
	private String contentList2[] = { "1", "2", "3", "4", "5" };
	//private ArrayList<String> contentList2 = new ArrayList<String>();
	private Button addButton;
	private Button sortButton;
	private EditText editText;
	private ArrayAdapter<String> listAdapter1 = null;
	private ArrayAdapter<String> listAdapter2 = null;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listView1 = (ListView) findViewById(R.id.listView1);
		listAdapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, contentList1);
		listView1.setAdapter(listAdapter1);
		listView1.setOnItemClickListener(this);
		
		listView2 = (ListView) findViewById(R.id.listView2);
		listAdapter2 = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, contentList2);
		listView2.setAdapter(listAdapter2);
		
		listView2.setOnItemClickListener(this);
		
		addButton = (Button) findViewById(R.id.button1);
		addButton.setOnClickListener(this);
		
		sortButton = (Button) findViewById(R.id.btn_sort_asc);
		sortButton.setOnClickListener(this);
		
		editText = (EditText) findViewById(R.id.editText1);
		

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		final String item = (String) parent.getItemAtPosition(position);

		if (parent.getId() == R.id.listView1) {
			Toast.makeText(this, "listView1: " + item, Toast.LENGTH_SHORT)
					.show();
		} else if (parent.getId() == R.id.listView2) {
			Toast.makeText(this, "listView2: " + item, Toast.LENGTH_SHORT)
					.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button1) {
			String text = editText.getText().toString().trim();
			if (!text.equals("")) {
				contentList1.add(text);
				listAdapter1.notifyDataSetChanged();
				editText.setText("");
			}
		} 
		else if (v.getId() == R.id.btn_sort_asc) {
			Collections.sort(contentList1);
			listAdapter1.notifyDataSetChanged();
		}
		
	}

}
