package de.htw.berlin.filedemo1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FileDemo1 extends Activity implements OnClickListener {

	static final String TAG = FileDemo1.class.getSimpleName();
	
	private TextView textView;
	private EditText editText;
	
	private static final String FILENAME = TAG + ".txt";
	private static String ABSOLUTE_FILENAME; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_demo1);
		editText = (EditText) findViewById(R.id.editText1);
		textView = (TextView) findViewById(R.id.txt_preview);
		Button btn_save = (Button) findViewById(R.id.btn_save);
		Button btn_load = (Button) findViewById(R.id.btn_load);
		Button btn_dele = (Button) findViewById(R.id.btn_delete);
		
		btn_save.setOnClickListener(this);
		btn_load.setOnClickListener(this);
		btn_dele.setOnClickListener(this);
		
		//nur ein Log:
		File file = getFilesDir();
		Log.d(TAG, file.getAbsolutePath());
		ABSOLUTE_FILENAME = getFilesDir() + File.separator + FILENAME;
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.file_demo1, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_save) {
			String edited = editText.getText().toString();
			save( edited +'\n');
			editText.setText("");
		} else if (v.getId() == R.id.btn_load) {
			String loaded = load(FILENAME);
			textView.setText(loaded);
		} else if (v.getId() == R.id.btn_delete) {
			File file = new File(ABSOLUTE_FILENAME);
			
			Log.d(TAG, file.getAbsolutePath());
			//File file = new File(FILENAME);
			boolean deleted = file.delete();
			Toast.makeText(this,"file deleted: " + deleted, Toast.LENGTH_LONG).show();
		}
	}

	private String load(String filename) {
		StringBuilder stringBuilder = new StringBuilder();
		FileInputStream fileInStream = null;
		InputStreamReader inStreamReader = null;
		BufferedReader bufReader = null;
		
		try {
			fileInStream = openFileInput(filename);
			inStreamReader = new InputStreamReader(fileInStream);
			bufReader = new BufferedReader(inStreamReader);
			String s;
			while ( (s = bufReader.readLine())!= null ) {
				if (s.length()!= 0) {
					stringBuilder.append('\n');
				}
				stringBuilder.append(s);
			}
			stringBuilder.append('\n');
		} catch (Exception e) {
			Log.e(TAG, "load error", e);
		} finally { 
			if (fileInStream != null) {
				try {
					fileInStream.close();
				} catch (IOException e) {
					Log.e(TAG, "load fileInstream.close() error", e);
				}
			}
			if (inStreamReader != null){
				try {
					inStreamReader.close();
				} catch (IOException e) {
					Log.e(TAG, "load inStreamReader.close() error", e);
				}
			}
			if (bufReader != null) {
				try {
					bufReader.close();
				} catch (IOException e) {
					Log.e(TAG, "load bufReader.close() error", e);
				}
			}
		}
		return stringBuilder.toString();
	}

	private void save(String text)  {
		FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		try {
			fos = openFileOutput(FILENAME, MODE_PRIVATE|MODE_APPEND );
			osw = new OutputStreamWriter(fos);
			osw.write(text);
		} catch (Exception e) {
			Log.e(TAG, "save failed", e);
		} finally {
			if (osw != null) {
				try {
					osw.close();
				} catch (IOException e2) {
					//e2.printStackTrace();
					Log.e(TAG, "osw.close failed", e2);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e2) {
					//e2.printStackTrace();
					Log.e(TAG, "fos.close failed", e2);
				}
			}
			
		}
	}
}
