package de.htw_berlin.gma.aufgabe03;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;
import java.util.Random;


public class RandomActivity extends AppCompatActivity {

    private TextView tvResult;
    private ImageView ivResult;
    private Bitmap bitmaps[] = new Bitmap[6];
    private int counter = 0;
    private TextView tvCounter = null;
    private ToneGenerator tg;
    private AnimationDrawable animatedCube;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random);
        tvResult = (TextView)this.findViewById(R.id.tv_random_result);
        ivResult = (ImageView)this.findViewById(R.id.imageView_ResultNumber);
        //start comment 1
        if ( ivResult.getDrawable() instanceof AnimationDrawable) {
            animatedCube =  (AnimationDrawable) ivResult.getDrawable();
            Handler handler = new Handler(getMainLooper());
            handler.postDelayed(new Runnable(){

                @Override
                public void run() {
                    animatedCube.start();
                }

            }, 1500);
        }
        // end  comment 1
        tvCounter = (TextView)this.findViewById(R.id.tvCount);
        tg = new ToneGenerator(AudioManager.STREAM_SYSTEM, 100);


        bitmaps[0] = BitmapFactory.decodeResource(getResources(), R.drawable.wuerfel_zahl_1);
        bitmaps[1] = BitmapFactory.decodeResource(getResources(), R.drawable.wuerfel_zahl_2);
        bitmaps[2] = BitmapFactory.decodeResource(getResources(), R.drawable.wuerfel_zahl_3);
        bitmaps[3] = BitmapFactory.decodeResource(getResources(), R.drawable.wuerfel_zahl_4);
        bitmaps[4] = BitmapFactory.decodeResource(getResources(), R.drawable.wuerfel_zahl_5);
        bitmaps[5] = BitmapFactory.decodeResource(getResources(), R.drawable.wuerfel_zahl_6);

    }

    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.imageButton_generateRandom:
                ++counter;
                tvCounter.setText(""+counter);
                tg.startTone(ToneGenerator.TONE_CDMA_ALERT_AUTOREDIAL_LITE, 100);
                Random rand = new Random();
                int limit = 6;
                int generatedRand = rand.nextInt(limit) +1;
                String randString = String.format( Locale.getDefault(),"result is: %d", generatedRand);
                tvResult.setText(randString);
                //start comment 2
                if (generatedRand >0 && generatedRand <7) {
                    ivResult.setImageBitmap(bitmaps[generatedRand-1]);
                } else {
                    ivResult.setImageResource(R.drawable.ic_launcher);
                }
                //end comment 2
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_random, menu);
        return true;
    }

}
