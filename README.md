#GMA - Grundlagen mobiler Anwendungen.

Die Aufgaben hier habe ich im Rahmen der gleichnamigen Vorlesung an der HTW Berlin im Wintersemester 2012/13 für die Übung entwickelt. 
Ziel war es, möglichst kleine überschaubare Einheiten zu schaffen, damit diese leicht im eigenen Kontext beliebig kombiniert werden können. Bis zu Version 18 des Android SDK war das "ApiDemos"-Sample-Projekt Bestandteil der mit ausgelieferten Samples. 
In diesem Beispielprojekt wurde alles in einer einzigen App zusammengetragen, was es neues bei Android gab. Für Neulinge in der Android-Programmierung bzw. Programmieranfänger ist es nicht so leicht, sich daraus das Thema zu extrahieren, für das man sich gerade interessiert. Zudem kann es auch nicht schaden, eine gewisse Routine im Erstellen von Projekten zu gewinnen. Daher habe ich mich dazu entschlossen die Aufgaben in jeweils eigene neue Apps aufzuteilen.

Die konkreten Aufgabenstellungen finden Sie in der Datei aufgaben.fodt hier in diesem Repository. Diese können Sie mit OpenOffice öffnen. Hier folgt nun eine Inhaltsangabe der Aufgaben und Lösungen

### Inhalstverzeichnis


#####Aufgabe 01 ("Erste Begegnung")
Alles ist neu und ungewohnt. Daher können wir schon stolz sein, eine erste App zu erstellen, wenn die Entwicklungsumgebung erst einmal läuft. Hier sehen wir zum ersten mal, wie Android-Projekte aufgebaut sind und wie z.B. Stringresourcen angelegt werden, wie die grafische Oberläche einer Activity bearbeitet wird usw.

#####Aufgabe 02 (erste Würfel App)
Wir reagieren auf einen Button-Klick und erzeugen eine Zufallszahl, die wir ausgeben in einem TextView. Ebenso lernen wir aus einem Eingabefeld die eingegebenen Werte auszulesen.

#####Aufgabe 03 (eine "schönere" Würfel-App)
Wir fügen hier der App ein eigenes Launcher-Icon hinzu mit den richtigen Auflösungen für die entsprechenden Geräte. Wir verwenden einen Image-Button und ein ImageView und können die Bilder zur Laufzeit verändern je nach Würfelergebnis.
Wir erstellen eine Progressbar (horizontal) und zeigen die Würfelergebnisse auf der Progressbar an.
Zusätzlich können wir einen Ton beim Button-Klick erzeugen.

#####Aufgabe 04 (Mehrseitige App mit explizitem Intent)
Hier erfahren Sie, wie sie Apps mit mehreren Activities erstellen und von einer "Seite" zur nächsten gelangen. Hier nutzen wir "explizite Intents". Wir können hier beobachten, wie sich die Up-Logik von der Back-Logik unterscheidet (s. Zusatz)

#####Aufgabe 05 (Mehrseiteige App - Daten weiterreichen)
Hier lernen Sie wie Sie bei mehrseitigen Apps einfache Daten von einer Activity an die nächste weitergeben können. Ebenso erfahren Sie, wie Sie beim Beenden einer Activity einfache Daten an die aufrufende Activity zurückgeben können.

#####Aufgabe 06 (andere bestehende Apps starten über implizite Intents)
Hier lernen Sie, wie Sie folgende Funktionen Ihres Androidgerätes aus Ihrer App starten können: „Telefonanruf“, „SMS versenden“, „Url im Browser öffnen“, „Email-Client starten“, „Map an vorgegebenem Ort öffnen“, „Navigationsapp starten“, „Streetview für eine Position anzeigen“.

#####Aufgabe 07 (wie Sie einen eigenen Intent erstellen und aufrufen)
"Veröffentlichen" Sie einen einen eigenen Intent im Manifest und geben anderen Entwicklern damit Zugriffsmöglichkeit auf Ihre App. Jetzt können damit Ihre Apps miteinander kommunizieren.

#####Aufgabe 8 ( einfache Listen-App)
Viele Apps, wenn nicht die meisten, nutzen Listen. Diesmal widmen wir uns einem einfachen Aufbau einer Liste.

#####Aufgabe 09 (nur einzelne Bereiche sind Listen)
Wenn Sie weitere Elemente, wie Buttons oder Textfelder haben möchten, leiten Sie nicht von ListActivity ab.

#####Aufgabe 10 (Schreiben und Lesen von Dateien)
Üben Sie Dateizugriffe in Android

#####Aufgabe 11 (SQLite Beispiel)
Wir erstellen eine einfache Tabelle über ADB (Android Debug Bridge) und dann aus einer App heraus und fügen über die App Daten hinzu.

#####Aufgabe 12 ViewExtension
Diesmal erweitern wir die View-Klasse und können so interessante eigene Elemente erstellen. Im zweiten Schritt werden wir diese ViewExtension animieren.

#####Aufgabe 13 Reaction-View
Mit dem bisher gelernten entwickeln wir eine erste Version eines Reaktions-Spiels.

#####Aufgabe 14 Reaction-Game with Prefs
In dieser Aufgabe erweitern wir das Reaktions-Spiel um eine Preferences-Activity. Überdies lernen wir Einstellungen persistent zu speichern.

Die Idee des Reaktions-Spiels stammt aus dem Buch von Dirk Louis und Peter Müller "Android" 2014 bei Hanser: Print-ISBN: 978-3-446-43823-1, E-Book-ISBN: 978-3-446-43831-6. (Das ist eine Neuauflage des Buches: "Jetzt lerne ich Android", Verlag Markt & Technik)
Autoren-Webseite www.carpelibrum.de

#####Aufgabe 15 
Fragmente - Master-Detail-View

#####Aufgabe 16
Networking with AsyncTask. 
Siehe auch das Beispiel ThreadedDownloads in dem Repo:
https://github.com/merkury/LiveLessons.git

#####Aufgabe 17
Handler example. 
Siehe auch das Beispiel ThreadedDownloads in dem Repo:
https://github.com/merkury/LiveLessons.git

#####Aufgabe 18
Bound Service example

#####Aufgabe 19
Mit API-Level 23 (Android 6.0 Marshmallow) wurde ein neues Berechtigungssystem eingeführt. Für Geräte mit Android >= 6.0 müssen Berechtigungen zur Laufzeit abgefragt werden und nicht mehr bei der Installation.
Es entstand die Frage, wie man mehrere Berechtigungen abfragen kann und wann man weiss, ob alle Berechtigungen erteilt wurden. In dem Artikel http://stackoverflow.com/questions/34342816/android-6-0-multiple-permissions finden Sie, wie man mit einem Aufruf mehrere Berechtigungen gleichzeitig abfragt und nach allen User-Eingaben auch nur einen Aufruf von **onRequestPermissionsResult()** bekommt. Hier erhalten Sie dann alle erteilten Permissions in einem Array. Dieses Array enthält dann die Antworten des Nutzers in der selben Reihenfolge, wie im Array, das dem Aufruf von **requestPermissions()** mitgegeben wurde.

Es werden insgesamt 4 Permissions angefragt, aber nur 3 Dialoge gezeigt. Das liegt daran, dass zwei Permissions aus der Gruppe Location kommen und beide als "dangerous" eingestuft sind. Dann fragt das Android-System nur eine der beiden ab. Wird sie vom Nutzer erlaubt, sind automatisch beide erlaubt.

In dem vorliegenden Code funktioniert die State-Restauration leider nicht! Sie können also nicht das Device drehen, während die Permissions-Abfrage-Dialoge sichtbar sind. Die Dialoge werden sonst mehrfach gezeigt.