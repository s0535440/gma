package de.htw_berlin.gma.handlerexample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

//import android.os.Handler;


public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ProgressBar progress;
    private TextView text;
    private Button startButton;
    private static final int SLEEP_TIME = 1000;
    private static final int LOOPS = 10;
    //private Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        startButton = (Button) findViewById(R.id.button);
        text = (TextView) findViewById(R.id.textView);
        //handler = new Handler();
    }

    public void startProgress(View view) {
        if (view.getId() == R.id.button) {
            startButton.setEnabled(false);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < LOOPS; i++) {
                        final int value = i;
                        doTimeConsumingWork();
                        //handler.post(new Runnable() {
                        progress.post(new Runnable() {
                            //handler.post(new Runnable() {
                            @Override
                            public void run() {
                                text.setText(R.string.updating);
                                progress.setProgress(value + 1);
                                startButton.setEnabled(true);
                            }
                        });
                    }

                }
            };
            new Thread(runnable).start();
        }
    }

    //Time consuming work can be a download etc...
    private void doTimeConsumingWork() {
        try {
            Thread.sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
